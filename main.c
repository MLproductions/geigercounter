#include "mcc_generated_files/mcc.h"
#define FCY 16000000UL //Unsigned Long
#include <libpic30.h>
#include "OLED_I2C.h"
#include <string.h>
#include <math.h>

uint32_t Intervals[8];
double IntMedio = 0;
int x = 0;
int particles;
double CPM = 0;
double Inst_CPM = 0;
char txt1[16] ={'C','P','M','_','a','v','r','g',':'};
char txt2[16] ={'C','P','M','_','i','n','s','t',':'};
char txt3[16] ={'C','O','U','N','T','S',':'};

void reverse(char *str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char *res, int afterpoint);

int main(void)
{
    SYSTEM_Initialize();
    OLED_Init();
    __delay_ms(500);
    OLED_InvertDisplay(0x01);
    __delay_ms(500);
    OLED_ClearDisplay();
    OLED_InvertDisplay(0x00);
    TMR2_Start();
    INTERRUPT_Initialize();
    
    while (1)
    {
        IntMedio = 0;
        for (x = 0; x < 8; x++){
            IntMedio += Intervals[x];
        }
        CPM = 3840000000.0f / IntMedio;
        Inst_CPM = 480000000.0f / Intervals[0];
        char Avrg_CPM_str[10];
        ftoa(CPM, Avrg_CPM_str,4);
        char Inst_CPM_str[10];
        ftoa(Inst_CPM, Inst_CPM_str,4);
        char time_str[10];
        intToStr(particles,time_str,0);
        //print CPM su i2c
        //__delay_ms(500);
        PORTBbits.RB10 = 0;
        OLED_ClearDisplay();
        //OLED_Update();
        OLED_Write_Text(4,24,txt3); 
        OLED_Write_Text(RIGHT,24,time_str);
        OLED_Write_Text(4,12,txt2);  
        OLED_Write_Text(RIGHT,12,Inst_CPM_str);
        OLED_Write_Text(4,0,txt1);
        OLED_Write_Text(RIGHT,0,Avrg_CPM_str);
        OLED_Update();
        Idle();        
    }
    
    return -1;
}

// reverses a string 'str' of length 'len' 
void reverse(char *str, int len) 
{ 
    int i=0, j=len-1, temp; 
    while (i<j) 
    { 
        temp = str[i]; 
        str[i] = str[j]; 
        str[j] = temp; 
        i++; j--; 
    } 
} 

int intToStr(int x, char str[], int d) 
{ 
    int i = 0; 
    while (x) 
    { 
        str[i++] = (x%10) + '0'; 
        x = x/10; 
    } 
  
    // If number of digits required is more, then 
    // add 0s at the beginning 
    while (i < d) 
        str[i++] = '0'; 
  
    reverse(str, i); 
    str[i] = '\0'; 
    return i; 
} 

// Converts a floating point number to string. 
void ftoa(float n, char *res, int afterpoint) 
{ 
    // Extract integer part 
    int ipart = (int)n; 
  
    // Extract floating part 
    float fpart = n - (float)ipart; 
  
    // convert integer part to string 
    int i = intToStr(ipart, res, 0); 
  
    // check for display option after point 
    if (afterpoint != 0) 
    { 
        res[i] = '.';  // add dot 
  
        // Get the value of fraction part upto given no. 
        // of points after dot. The third parameter is needed 
        // to handle cases like 233.007 
        fpart = fpart * pow(10, afterpoint); 
  
        intToStr((int)fpart, res + i + 1, afterpoint); 
    } 
} 